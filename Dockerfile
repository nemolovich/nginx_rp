FROM nginx:1.13-alpine

# NOT FOUND response
COPY ./backend-not-found.html /var/www/backend-not-found.html

#  Proxy and SSL configurations
COPY ./includes/ /etc/nginx/includes/

# Proxy SSL certificates
COPY ./ssl/ /etc/nginx/ssl/

EXPOSE 80
EXPOSE 443
