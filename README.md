# Reverse Proxy server on NGINX

Use NGINX on host network for reverse proxy.

## With standard docker

### Pull from Docker Hub

Pull image with:

```
docker pull nemolovich/nginx_rp:v1.0.1
```

_NB_: Available tags on: https://cloud.docker.com/repository/docker/nemolovich/nginx_rp/tags


### Build

Build the NGINX container with:

```
docker build -t nemolovich/nginx_rp ./
```


### Run

```
docker run -d --name nginx_rp -v /opt/volumes/nginx/conf:/etc/nginx/conf.d:Z \
   -v /opt/volumes/nginx/logs_rp:/var/log/nginx:Z \
   -v /opt/volumes/nginx/certs:/etc/nginx/ssl:Z \
   -v /opt/volumes/nginx/public_html:/var/www/html:Z \
   --network host nemolovich/nginx_rp:v1.0.1
```

Put your nginx configuration files into the *conf* volume.


## With docker-compose

```
docker-compose up
```

The compose configuration is the following:

* Volumes:
  - Config directory: **/etc/nginx/conf.d**
  - Static files directory: **/var/www/html**
  - Logs directory: **/var/log/nginx**
  - Certificates directory: **/etc/nginx/ssl**
* Network mode: **host**

## Configuration

In order to add a proxy redirection you have to create a config file like
[conf example](./default.conf) and put it inside the *conf* volume.

### Add a redirection

To add a route redirection, add the following block inside your configuration
(inside **server** block):
```
    location /<ROUTE>/ {
        include /etc/nginx/includes/proxy.conf;
        rewrite ^/<ROUTE>/?(.*)$ /$1 break;
        proxy_pass  http://127.0.0.1:<PORT>;
    }
```

Where the parameters are:
* **&lt;ROUTE>**: The context path to access on application
* **&lt;PORT>**: The application redirection port

## Access to web site

You can now access to your applications from the NGINX docker with your host 
IP on port 80.

eg: http://localhost
